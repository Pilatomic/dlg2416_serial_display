#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdint.h>
#include <string.h>

#define DISPLAY_CHAR_COUNT  16
#define UART_BAUDRATE       9600

#define D0_PORT     D
#define D0_PIN      1<<1
#define D1_PORT     D
#define D1_PIN      1<<2
#define D2_PORT     D
#define D2_PIN      1<<4
#define D3_PORT     C
#define D3_PIN      1<<5
#define D4_PORT     C
#define D4_PIN      1<<2
#define D5_PORT     C
#define D5_PIN      1<<3
#define D6_PORT     C
#define D6_PIN      1<<4
#define A0_PORT     D
#define A0_PIN      1<<5
#define A1_PORT     D
#define A1_PIN      1<<6
#define CU_PORT     B
#define CU_PIN      1<<1
#define CUE_PORT    B
#define CUE_PIN     1<<2
#define WR1_PORT    B
#define WR1_PIN     1<<0
#define WR2_PORT    D
#define WR2_PIN     1<<7
#define WR3_PORT    C
#define WR3_PIN     1<<0
#define WR4_PORT    C
#define WR4_PIN     1<<1
#define BL_PORT     D
#define BL_PIN      1<<3
#define CLR_PORT    B
#define CLR_PIN     1<<4

#define CAT(x,y) x##y
#define PIN_LOW(P,x)  (CAT(PORT,P) &= ~(x))
#define PIN_HIGH(P,x) (CAT(PORT,P) |= (x))
#define PIN_OUT(P,x,b) ((b) ? PIN_HIGH(P,x) : PIN_LOW(P,x))
#define PIN_PULSE_LOW(P,x) PIN_LOW(P,x);PIN_HIGH(P,x)
#define SET_OUT(P,x) CAT(DDR,P) |= (x)

#define UBRR_VAL    (F_CPU/16/UART_BAUDRATE-1)

void writeChar(uint8_t i, uint8_t d)
{
    // Set A1 & A0 pins
    switch(i & 0x03)
    {
    case 0x00:
        PIN_HIGH(A1_PORT, A1_PIN);
        PIN_HIGH(A0_PORT, A0_PIN);
        break;
    case 0x01:
        PIN_HIGH(A1_PORT, A1_PIN);
        PIN_LOW(A0_PORT, A0_PIN);
        break;
    case 0x02:
        PIN_LOW(A1_PORT, A1_PIN);
        PIN_HIGH(A0_PORT, A0_PIN);
        break;
    case 0x03:
        PIN_LOW(A1_PORT, A1_PIN);
        PIN_LOW(A0_PORT, A0_PIN);
        break;
    default:
        break;
    }

    // Write data
    PIN_OUT(D0_PORT, D0_PIN, d & (1<<0));
    PIN_OUT(D1_PORT, D1_PIN, d & (1<<1));
    PIN_OUT(D2_PORT, D2_PIN, d & (1<<2));
    PIN_OUT(D3_PORT, D3_PIN, d & (1<<3));
    PIN_OUT(D4_PORT, D4_PIN, d & (1<<4));
    PIN_OUT(D5_PORT, D5_PIN, d & (1<<5));
    PIN_OUT(D6_PORT, D6_PIN, d & (1<<6));

    // Strobe WR pin
    switch(i & 0x0C)
    {
    case (0x00<<2):
        PIN_PULSE_LOW(WR1_PORT, WR1_PIN);
        break;

    case (0x01<<2):
        PIN_PULSE_LOW(WR2_PORT, WR2_PIN);
        break;

    case (0x02<<2):
        PIN_PULSE_LOW(WR3_PORT, WR3_PIN);
        break;

    case (0x03<<2):
        PIN_PULSE_LOW(WR4_PORT, WR4_PIN);
        break;

    default:
        break;
    }
}

volatile uint8_t displayBuffer[DISPLAY_CHAR_COUNT];
volatile uint8_t displayDirty;
volatile uint8_t writeIndex;

void setupPins()
{
    //Clr display before anything
    SET_OUT(CLR_PORT, CLR_PIN);
    PIN_LOW(CLR_PORT, CLR_PIN);

    SET_OUT(D0_PORT, D0_PIN);
    SET_OUT(D1_PORT, D1_PIN);
    SET_OUT(D2_PORT, D2_PIN);
    SET_OUT(D3_PORT, D3_PIN);
    SET_OUT(D4_PORT, D4_PIN);
    SET_OUT(D5_PORT, D5_PIN);
    SET_OUT(D6_PORT, D6_PIN);
    SET_OUT(A0_PORT, A0_PIN);
    SET_OUT(A1_PORT, A1_PIN);
    SET_OUT(CU_PORT, CU_PIN);
    SET_OUT(CUE_PORT, CUE_PIN);
    SET_OUT(WR1_PORT, WR1_PIN);
    SET_OUT(WR2_PORT, WR2_PIN);
    SET_OUT(WR3_PORT, WR3_PIN);
    SET_OUT(WR4_PORT, WR4_PIN);
    SET_OUT(BL_PORT, BL_PIN);

    PIN_HIGH(CU_PORT, CU_PIN);
    PIN_HIGH(WR1_PORT, WR1_PIN);
    PIN_HIGH(WR2_PORT, WR2_PIN);
    PIN_HIGH(WR3_PORT, WR3_PIN);
    PIN_HIGH(WR4_PORT, WR4_PIN);
    PIN_HIGH(CLR_PORT, CLR_PIN);
}

void clearBuffer()
{
    for(uint8_t i = 0 ; i < DISPLAY_CHAR_COUNT ; i++)
    {
        displayBuffer[i] = ' ';
    }
}

void setupUart()
{
    /*Set baud rate */
    UBRR0H = (uint8_t)(UBRR_VAL>>8);
    UBRR0L = (uint8_t)UBRR_VAL;

    // 8N1, RX-Only with IT
    UCSR0A = 0x00;
    UCSR0B = (1<<RXEN0) | (1<<RXCIE0);
    UCSR0C = (3<<UCSZ00);
}

void setupBlankingPwm()
{
    // Fast PWM with output on OC2B
    TCCR2A = (2<<COM2B0) | (3<<WGM20);

    // Prescaler div by 1 ( ~32kHz blanking )
    TCCR2B = (1<<CS20);

    // Full Brightness
    OCR2B = 128;

    // All interrupts disabled
    TIMSK2 = 0x00;

}

int main()
{
    writeIndex = 0;
    displayDirty = 1;
    clearBuffer();
    setupUart();
    setupPins();
    setupBlankingPwm();

    sei(); // Enable interrupts

    while(1)
    {
        if(displayDirty)
        {
            displayDirty = 0;
            for(uint8_t i = 0 ; i < DISPLAY_CHAR_COUNT ; i++)
            {
                writeChar(i,displayBuffer[i]);
            }
        }
    }
}

ISR(USART_RX_vect)
{
    uint8_t rxChar = UDR0;

    // Control chars
    if(rxChar < 0x20)
    {
        switch(rxChar)
        {
        case '\n':
            clearBuffer();
            displayDirty = 1;
            writeIndex = 0;
            break;

        case '\r':
            writeIndex = 0;
            break;

        case '\b':
            if(writeIndex > 0) writeIndex--;
            break;

        default:
            break;
        }
    }

    // Display chars and some room in display buffer
    else if(rxChar < 0x7F && writeIndex < DISPLAY_CHAR_COUNT)
    {
        displayBuffer[writeIndex++] = rxChar;
        displayDirty = 1;
    }
}
